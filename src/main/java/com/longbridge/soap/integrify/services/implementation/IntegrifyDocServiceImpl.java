package com.longbridge.soap.integrify.services.implementation;

import com.longbridge.soap.integrify.models.IntegrifyRequest;
import com.longbridge.soap.integrify.models.ServiceResponse;
import com.longbridge.soap.integrify.services.IntegrifyDocService;
import com.longbridge.console.doc.integrify_finacle_service.GetIntegrifyDocRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by B. Programmer on 28/12/2018.
 */
@Service
public class IntegrifyDocServiceImpl implements IntegrifyDocService {

    private static final Logger logger = LoggerFactory.getLogger(IntegrifyDocServiceImpl.class);

    /***
     * This method is to get the updated documents from Integrify and do necessary update on Finacle.
     * This method calls a FI webservice from Finacle application to do the update
     * @return
     */
    @Override
    public ServiceResponse updateFinacleDocumentsFromIntegrify(GetIntegrifyDocRequest postRequest){
        ServiceResponse serviceResponse = new ServiceResponse();
        if(postRequest != null){
            String response = updateDocumentsOnFinacle(postRequest);
            logger.info("response from rest service is: {}", response);
            if(response.equals("Success")){
                serviceResponse.setStatus("SUCCESS");
                serviceResponse.setMessage("OK");
            }
            else {
                serviceResponse.setStatus("FAILURE");
                serviceResponse.setMessage(response);
            }
            return serviceResponse;
        }
        else {
            serviceResponse.setStatus("FAILURE");
            serviceResponse.setMessage("Empty request payload");
        }
        return serviceResponse;
    }

    /***
     * This is the POST method the communicate to the REST service provided by BC application in order to update its documents
     * @return
     */
    @Override
    public String updateDocumentsOnFinacle(GetIntegrifyDocRequest postRequest) {
        return ProcessIntegrifyOnFIService.getInstance().sendRequestToFinacle((IntegrifyRequest) createIntegrifyRequest(postRequest)); //this is to post request to Finacle using FI
    }

    private Object createIntegrifyRequest(GetIntegrifyDocRequest postRequest) {
        IntegrifyRequest integrifyRequest = new IntegrifyRequest();
        integrifyRequest.setAccountNumber(postRequest.getAccountNumber());
        integrifyRequest.setAccountType(postRequest.getAccountType());
        integrifyRequest.setDocProvided(postRequest.getDocProvided());
        integrifyRequest.setSchemeCode(postRequest.getSchemeCode());
        integrifyRequest.setAccountCategory(postRequest.getAccountCategory());
        integrifyRequest.setDeferralInPlace(postRequest.getDeferralInPlace());
        integrifyRequest.setDeferralApprover(postRequest.getDeferralApprover());
        integrifyRequest.setDeferralExpiryDate(postRequest.getDeferralExpiryDate());
        logger.info("Request for the FI service is: {}", integrifyRequest);
        return integrifyRequest;
    }



}
