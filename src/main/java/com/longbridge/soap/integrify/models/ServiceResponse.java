package com.longbridge.soap.integrify.models;

/***
 *  Created by B. Programmer on 16/01/2019
 */

public class ServiceResponse {
    private String status;
    private String message;

    public ServiceResponse(){}

    public ServiceResponse(String status, String message){
        this.status = status;
        this.message =message;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
