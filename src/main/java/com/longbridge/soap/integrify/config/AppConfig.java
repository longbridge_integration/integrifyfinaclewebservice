package com.longbridge.soap.integrify.config;

import com.longbridge.soap.integrify.models.ServiceResponse;
import com.longbridge.soap.integrify.repository.ServiceStatusRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/***
 *  Created by B. Programmer on 15/01/2019
 */

@Configuration
public class AppConfig {
    @Bean
    public ServiceResponse serviceResponse() {
        return new ServiceResponse();
    }

    @Bean
    public RestTemplate rest() {
        return new RestTemplate();
    }

}
