package com.longbridge.soap.integrify.services;

public interface ProcessFIService<T> {
    String sendRequestToFinacle(T object);
}
