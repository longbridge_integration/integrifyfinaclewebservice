package com.longbridge.soap.integrify.config.fiservices;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyStore;


/**
 * @author B. Programmer
 * @Date: 08-01-19
 */
@Service
public class CustomHttpClient {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${FI.URL}")
    private String FI_SERVICE_URL;


    public CustomHttpClient() {

    }

    /**
     * This calls the FI
     * to execute the payments
     *
     * @return boolean
     */


    public String sendHttpRequest(String payload) {

        String action = "";
        logger.info("Calling FI_SERVICE with payload -- {} ", payload);

        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);



            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);


            DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);

            // CloseableHttpClient httpClient = HttpClients.createDefault();

            HttpEntity httpEntity = new ByteArrayEntity(payload.getBytes("UTF-8"));

            HttpPost httpPost = new HttpPost(FI_SERVICE_URL);
            httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");
            httpPost.addHeader("SOAPAction", action);


            httpPost.setEntity(httpEntity);

            HttpResponse response = httpClient.execute(httpPost);

            String responseMessage = EntityUtils.toString(response.getEntity());
            responseMessage = org.apache.commons.lang3.StringEscapeUtils.unescapeXml(responseMessage);

            return responseMessage;

        } catch (Exception e) {
            logger.error("Problem occurred in reaching service", e);
        }

        return "NO RESPONSE";

    }

}
