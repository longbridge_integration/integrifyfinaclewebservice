package com.longbridge.soap.integrify.services;

import com.longbridge.soap.integrify.models.ServiceResponse;
import com.longbridge.soap.integrify.repository.ServiceStatusRepo;
import com.longbridge.console.doc.integrify_finacle_service.GetIntegrifyDocRequest;

/**
 * Created by B. Programmer on 28/12/2018.
 */
public interface IntegrifyDocService {
    ServiceResponse updateFinacleDocumentsFromIntegrify(GetIntegrifyDocRequest postRequest);
    String updateDocumentsOnFinacle(GetIntegrifyDocRequest postRequest);

}