package com.longbridge.soap.integrify.services.implementation;

import com.longbridge.soap.integrify.config.fiservices.CustomHttpClient;
import com.longbridge.soap.integrify.models.IntegrifyRequest;
import com.longbridge.soap.integrify.services.ProcessFIService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProcessIntegrifyOnFIService implements ProcessFIService<IntegrifyRequest> {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public static ProcessIntegrifyOnFIService instance = new ProcessIntegrifyOnFIService();

    @Autowired
    private CustomHttpClient customHttpClient;

    @Autowired
    private TemplateEngine templateEngine;


    @Override
    public String sendRequestToFinacle(IntegrifyRequest object){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Context context = new Context();
        context.setVariable("MessageDateTime", dateFormat.format(new Date()));
        context.setVariable("accountNumber", object.getAccountNumber());
        context.setVariable("accountType", object.getAccountType());
        context.setVariable("docProvided", object.getDocProvided());
        context.setVariable("schemeCode", object.getSchemeCode());
        context.setVariable("accountCategory", object.getAccountCategory());
        context.setVariable("deferralInPlace", object.getDeferralInPlace());
        context.setVariable("deferralApprover", object.getDeferralApprover());
        context.setVariable("deferralExpiryDate", object.getDeferralExpiryDate());


        String result = templateEngine.process("integrifyFIScript", context);
        logger.info("The Integrify request payload is {}",result);
        logger.trace("Requesting Integrify Details");
        System.out.println("Resulllltttttttttt:  " +result);

        String responseMessage = customHttpClient.sendHttpRequest(result);
        System.out.println("responseMessage: "+responseMessage);
        String charSequence1 = "<Status>SUCCESS</Status>";

        boolean isSuccessful = responseMessage.contains(charSequence1);
        String resultMsg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        String successOrFailure = StringUtils.substringBetween(responseMessage, "<SuccessOrFailure>", "</SuccessOrFailure>");

        if (isSuccessful) {
            System.out.println("resultMsg: " + resultMsg);
            if (successOrFailure =="Success") {
                System.out.println("resultMsg: Sent Successfully");
                resultMsg = successOrFailure;
            } else {
                resultMsg = StringUtils.substringBetween(responseMessage, "<Error_1>", "</Error_1>");
                System.out.println(resultMsg);
            }
        }
        else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            resultMsg = errorCode+" | "+errorDesc;
            System.out.println(resultMsg);
        }

        return resultMsg;
    }

    public static ProcessIntegrifyOnFIService getInstance(){
        if(instance == null){
            instance = new ProcessIntegrifyOnFIService();
        }
        return instance;
    }
}
