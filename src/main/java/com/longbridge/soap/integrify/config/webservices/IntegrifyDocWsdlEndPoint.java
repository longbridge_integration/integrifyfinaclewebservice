package com.longbridge.soap.integrify.config.webservices;

import com.longbridge.soap.integrify.models.ServiceResponse;
import com.longbridge.soap.integrify.repository.ServiceStatusRepo;
import com.longbridge.soap.integrify.services.IntegrifyDocService;
import com.longbridge.console.doc.integrify_finacle_service.GetIntegrifyDocRequest;
import com.longbridge.console.doc.integrify_finacle_service.GetIntegrifyDocResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by B. Programmer on 28/12/2018.
 */
@Endpoint
public class IntegrifyDocWsdlEndPoint {
    private static final String NAMESPACE_URI = "http://console.longbridge.com/doc/integrify-finacle-service";
    private static final Logger logger = LoggerFactory.getLogger(IntegrifyDocWsdlEndPoint.class);

    @Autowired
    private IntegrifyDocService integrifyDocService;

    @Autowired
    private ServiceResponse serviceResponse;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getIntegrifyDocRequest")
    @ResponsePayload
    public GetIntegrifyDocResponse getServiceStatus(@RequestPayload GetIntegrifyDocRequest request) {
        GetIntegrifyDocResponse response = new GetIntegrifyDocResponse();
        logger.info("IntegrifyDocRequest payload from SOAP service: {}", request);
        serviceResponse = integrifyDocService.updateFinacleDocumentsFromIntegrify(request);
        response.setServiceStatus(ServiceStatusRepo.getServiceStatus(serviceResponse));

        return response;
    }
}
