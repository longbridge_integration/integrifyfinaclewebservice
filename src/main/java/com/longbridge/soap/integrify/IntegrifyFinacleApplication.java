package com.longbridge.soap.integrify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrifyFinacleApplication {
	private static final Logger logger = LoggerFactory.getLogger(IntegrifyFinacleApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(IntegrifyFinacleApplication.class, args);
		logger.info("Web service has started");
	}

}

