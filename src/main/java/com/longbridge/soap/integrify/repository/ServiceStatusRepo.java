package com.longbridge.soap.integrify.repository;

import com.longbridge.console.doc.integrify_finacle_service.ServiceStatus;
import com.longbridge.console.doc.integrify_finacle_service.ConsoleStatus;
import com.longbridge.soap.integrify.models.ServiceResponse;

/**
 * Created by B. Programmer on 28/12/2018.
 */

public class ServiceStatusRepo {

    public static ServiceStatus getServiceStatus(ServiceResponse serviceResponse){
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatus(serviceResponse.getStatus());
        serviceStatus.setMessage(serviceResponse.getMessage());
        return serviceStatus;
    }

    public static ConsoleStatus getConsoleStatus(ServiceResponse serviceResponse){
        ConsoleStatus serviceStatus = new ConsoleStatus();
        serviceStatus.setStatus(serviceResponse.getStatus());
        serviceStatus.setMessage(serviceResponse.getMessage());
        return serviceStatus;
    }
}
